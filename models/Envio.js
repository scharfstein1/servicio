const { Sequelize } = require(".");

module.exports = (sequelize, DataTypes) => {

    const Envio = sequelize.define('Envio', {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        datos: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        endpoint: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        respuesta: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        fecha_envio: {
            type: DataTypes.DATE
        },
        fecha_respuesta: {
            type: DataTypes.DATE
        }

    });

    return Envio;
    
};
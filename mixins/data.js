'use strict';

const net = require('net');
const http = require('http'); 
const {db, Envio} = require('../models');
var querystring = require('querystring');

module.exports = {

    /**
     * Obtiene datos desde el sensor
     * @param {String} tipo 
     * @return {Promise}
     */
    get(tipo, host, port)
    {
        return new Promise(function (resolve, reject) {
            const client = new net.Socket();

            client.connect(
                port,
                host,
                function () {
                    client.write(tipo);
                }
            );

            client.on('data', function (data) {
                data = Number.parseFloat(data);
                resolve(data);
                client.destroy();
            })
                .on('close', function () {
                    // console.log('Connection closed');
                })
                .on('error', function (error) {
                    reject('Connection error ' + error);
                });

        });

    },

    /**
     * Entrega el promedio de los ultimos 5 y el promedio del total
     * @param {Array} data 
     * @returns {Object}
     */
    promediar(data) 
    {
        let promedio = 
            data.reduce(
                (total, temp) => total + temp
            ) / data.length;
        
        let cantidad = 5;
        let promedioUltimas =
            data
                .slice(-cantidad)
                .reduce(
                    (total, temp) => total + temp
            ) / cantidad;

        return {
            'cinco': promedioUltimas,
            'total': promedio
        };
    },

    /**
     * 
     * @param {Array} data Datos a procesar
     * @param {String} mensaje Mensaje a mostrar
     */
    procesar(tipo, data, mensaje) 
    {
        let promedios = this.promediar(data);
        
        if(promedios.cinco > promedios.total){
            console.log(mensaje);

            this.registrarValvulaAPI(tipo)
        }
    },

    /**
     * Envia los cambios en sensores hacia el API
     * @param {String} tipo 
     * @param {Double} datos
     * @param {String} respuesta 
     * @param {Date} fechaEnvio 
     * @param {Date} fechaRespuesta 
     */
    registrarSensorAPI(tipo = 'temperatura', datos)
    {
          
        let fechaEnvio = new Date();        
        let endpoint = process.env.API_REGISTRO_ENDPOINT_SENSOR;
        datos = JSON.stringify({
            tipo: tipo,
            valor: datos
        });

        let req = http.request({
            host: process.env.API_REGISTRO_HOST,
            port: process.env.API_REGISTRO_PORT,
            path: endpoint,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }
        }, res => {            
                this.guardarRegistroBD(datos, endpoint, res.statusCode, fechaEnvio);            

        });

        req.write(datos);
        req.end();

    },

    /**
     * Envia las aperturas de valvulas hacia el API
     * @param {String} tipo 
     * @param {Double} datos
     * @param {String} respuesta 
     * @param {Date} fechaEnvio 
     * @param {Date} fechaRespuesta 
     */
    registrarValvulaAPI(tipo = 'temperatura') {

        let fechaEnvio = new Date();
        let endpoint = process.env.API_REGISTRO_ENDPOINT_VALVULA;
        let datos = JSON.stringify({
            tipo: tipo
        });

        let req = http.request({
            host: process.env.API_REGISTRO_HOST,
            port: process.env.API_REGISTRO_PORT,
            path: endpoint,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }
        }, res => {
            this.guardarRegistroBD(datos, endpoint, res.statusCode, fechaEnvio);

        });

        req.write(datos);
        req.end();

    },

    /**
     * Guarda datos de envios al registro en base de datos
     * @param {String} datos 
     * @param {String} endpoint 
     * @param {String} respuesta 
     * @param {Date} fechaEnvio 
     */
    guardarRegistroBD(datos, endpoint, respuesta, fechaEnvio){

        Envio.create({
            'datos': datos,
            'endpoint': endpoint,
            'respuesta': respuesta,
            'fecha_envio': fechaEnvio,
            'fecha_respuesta': new Date()
        });

    },

}
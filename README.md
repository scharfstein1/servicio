Servicio

- Requerimientos
    - NodeJs
    - MariaDB
- Configuracion de base de datos
    - Crear base de datos llamada "registro"
- Instalacion
    - Duplicar archivo .env.example y renombrarlo a .env
        - Cambiar los siguientes campos en caso de haber cambiado los puertos en el proyecto Sensor:
            - SENSOR_TEMPERATURA_PORT
            - SENSOR_OXIGENO_PORT
    - Configurar credenciales de base de datos en caso de ser diferentes
    - npm install
- Iniciar
    - node start.js

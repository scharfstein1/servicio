'use strict';

require('dotenv').config();

const temperatura = require('./services/temperatura');
const oxigeno = require('./services/oxigeno');
const db = require('./models');

db
.sequelize
.sync()
.then((req) => {    
    temperatura.start();
    oxigeno.start();
});

'use strict';

const data = require('../mixins/data');

module.exports = {

    mensaje: 'Inyectando Oxígeno...',
    config: {},

    /**
     * Inicia servicio
     */
    start() 
    {
        if (process.env.SENSOR_OXIGENO_INTERVALO > 0) {
            this.obtenerOxigeno();
        }
    },

    /**
     * Obtiene Oxigeno
     */
    obtenerOxigeno()
    {
        let o2s = [];

        setInterval(() => {

            data.get('oxigeno', process.env.SENSOR_OXIGENO_HOST, process.env.SENSOR_OXIGENO_PORT)
                .then(o2 => {
                    o2s.push(o2);
                    data.registrarSensorAPI('oxigeno', o2);
                }, error => {
                    console.log(error);
                });

            if (o2s.length == 10) {
                data.procesar(
                    'oxigeno',
                    o2s, this.mensaje
                );
                o2s = [];
            }

        }, process.env.SENSOR_OXIGENO_INTERVALO * 1000)

    },



}
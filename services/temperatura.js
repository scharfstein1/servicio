'use strict';

const data = require('../mixins/data');

module.exports = {

    mensaje: 'Abriendo válvula de rellenado de agua...',
    config: {},

    /**
     * Inicia servicio
     */
    start()
    {        
        if (process.env.SENSOR_TEMPERATURA_INTERVALO > 0){
            this.obtenerTemperatura();
        }
    },

    /**
     * Obtiene temperaturas
     */
    obtenerTemperatura()
    {
        let temps = [];

        setInterval(() => {

            data.get('temperatura', process.env.SENSOR_TEMPERATURA_HOST, process.env.SENSOR_TEMPERATURA_PORT)
                .then((temp) => {
                    temps.push(temp);
                    data.registrarSensorAPI('temperatura', temp);
                }, error => {
                    console.log(error);
                });

            if (temps.length == 10) {
                data.procesar(
                    'temperatura',
                    temps, this.mensaje                    
                );
                temps = [];
            }

        }, process.env.SENSOR_TEMPERATURA_INTERVALO * 1000)

    },

    

}